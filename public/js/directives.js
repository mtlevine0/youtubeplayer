YoutubePlayer.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
        iElement.autocomplete({
        source: function(request, response) {
			$.getJSON("http://suggestqueries.google.com/complete/search?callback=?",
				{ 
				  "hl":"en", // Language
				  "ds":"yt", // Restrict lookup to youtube
				  "jsonp":"suggestCallBack", // jsonp callback function name
				  "q":scope.selected, // query term
				  "client":"youtube" // force youtube style response, i.e. jsonp
				}
			);
			suggestCallBack = function (data) {
				var suggestions = [];
				$.each(data[1], function(key, val) {
					suggestions.push({"value":val[0]});
				});
				suggestions.length = 10; // prune suggestions list to only 5 items
				response(suggestions);
			};
		},
		select: function(event, ui) {
                    $timeout(function() {
						iElement.trigger('input');
                    }, 0);
                }
		});
    };
});