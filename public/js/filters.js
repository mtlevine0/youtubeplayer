YoutubePlayer.filter('secToMin', function() {
	return function(input) {
		var waitTimeMin = Math.floor(input/60);
		var waitTimeSec = input - waitTimeMin * 60;
		if(waitTimeSec < 10){
				waitTimeSec = "0"+waitTimeSec;
		}
		return waitTimeMin + ":" + waitTimeSec;  
	};
});