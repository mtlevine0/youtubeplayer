var YoutubePlayer = angular.module('YoutubePlayer', []);
var suggestCallBack; // global var for autocomplete jsonp
var wait = 0;

function mainController($scope, $http) {
	$scope.videoForm = {};
	$scope.queryResults = '';
	
	// Fetch videoIds in the database every 500ms
	setInterval(function(){
		$http.get('/api/videos')
			.success(function(data) {
				$scope.video = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
			$scope.waitTime = 0;
			$.each( $scope.video, function( key, value ) {
  				$scope.waitTime += value.duration
			});
	},500);

	// Clear all videoIds from the database
	$scope.clearVideos = function() {
		var pass = prompt("Enter password to clear queue:");
		if (pass === "titties"){
			$http.delete('/api/videos/')
				.success(function(data) {
					console.log(data);
					$scope.video = data;
				})
				.error(function(data) {
					console.log('Error: ' + data);
				});
		}
	};
	
	// Delete a video from the queue
	$scope.deleteVideo = function(id) {
		var pass = prompt("Password:");
		if(pass === "baseball"){
			$http.delete('/api/videos/' + id)
				.success(function(data) {
					//$scope.videoIdOutput = data;
					$scope.video = data;
					console.log(data);
				})
				.error(function(data) {
					console.log('Error: ' + data);
				});
		}
	};
	
	// Add a video to the database
	$scope.selectVideo = function(result) {
		if(wait){
			$("#right-side").block({ 
				message: '<h3>Song Added. Please wait:</h3><h3 id="timer"></h3>' 
			});
		}
		$http.post('/api/videos', {videoId: result.id, title: result.title, duration: result.duration})
			.success(function(data) {
				$scope.videoForm = {};
				if(data.success === "true"){
					//$scope.videoIdOutput = data;
					$scope.video = data;
					if(wait){
						var count=30;
						var counter=setInterval(timer, 1000); 
						function timer(){
							count=count-1;
							if (count <= 0){
								clearInterval(counter);
								return;
							}
							$("#timer").html(count + " secs");
						}
						setTimeout(function() {
							$("#right-side").unblock();
						}, 30000);
					}
				}else{
					$("#right-side").block({ message: "Selection too long.  Max song length: 6:30" });
					setTimeout(function(){
						$("#right-side").unblock();
					},1500);
				}
				//$scope.videoIdOutput = data;
				$scope.video = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
				
			});					
	};
	
	// Search YouTube with given query and return results
	$scope.searchYoutube = function() {
		console.log("test");
		$("#output").scrollTop(0);
		var search_url = "http://gdata.youtube.com/feeds/api/videos?q="+$scope.selected+"&max-results=20&v=2&alt=jsonc&prettyprint=true";
		$.getJSON( search_url, function( json ) {
			$scope.queryResults = json.data.items;
		});
		$scope.selected = "";
		$( "input" ).autocomplete( "close" );
	};
}
