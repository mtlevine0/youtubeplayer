var mongoose = require('mongoose');

module.exports = mongoose.model('Video', {
	videoId: String,
	title: String,
	duration: Number,
});