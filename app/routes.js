var Video = require('./models/video.js');
var request = require('request');
var async = require('async');

module.exports = function(app) {
	// api ---------------------------------------------------------------------
	// Clear the entire message database
	app.delete('/api/videos/', function(req, res) {
		Video.remove(function(err, videos) {
			if (err)
				res.send(err);
			Video.find(function(err, videos) {
				if (err)
					res.send(err)
				res.json(videos);
			});
		});
	});
	
	// Delete a video from the queue database
	app.delete('/api/videos/:video_id', function(req, res) {
		Video.remove({
			_id : req.params.video_id
		}, function(err, todo) {
			if (err)
				res.send(err);

			// get and return all the todos after you create another
			Video.find(function(err, videos) {
				if (err)
					res.send(err)
				res.json(videos);
			});
		});
	});
	
	// Add a video to the database
	app.post('/api/videos', function(req, res) {
		console.log(req.body.videoId);
		if(req.body.duration < 390){
		Video.create({
			videoId : req.body.videoId,
			title: req.body.title,
			duration: req.body.duration,
			votes: 0
		}, function(err){
				if (err)
					res.json({success: "false"});
				else
					res.json({success: "true"});
		});
		}else{
			res.json({success: "false"});
		}
	});
	
	// Request that the next video be sent, request includes a paramater that specifies what is played next
	app.get('/api/handler', function(req, res) {
		Video.count(function(err, count) {
			if(count != 0){
				//Video.find().sort({'votes':'descending'}).limit(1).exec(function(err, video){
				Video.find(function(err, video){
					console.log(video[0].videoId);
					var returnScript = "<script>var player;function onYouTubeIframeAPIReady(){player=new YT.Player('player',{height:'390',width:'640',videoId:'"+video[0].videoId+"',playerVars:{'autoplay':1, 'controls':1},events:{'onReady':onReady}})}</script>";
					res.json(returnScript);
					Video.remove({
						videoId : video[0].videoId
					}, function(err, video){
						if (err)
							console.log(err);
							res.send(err);
					});
				});
				
			}else{
				res.json("<script>var player;function onYouTubeIframeAPIReady(){player=new YT.Player('player',{height:'390',width:'640',videoId:'U7f2GkTL8r8',playerVars:{'autoplay':0, 'controls': 1},events:{'onReady':onReady}})}</script>");
			}
		});
	});
	
	// Get all of the videos in the database
	app.get('/api/videos', function(req, res) {
		Video.find(function(err, video) {
			if (err)
				res.send(err)
			res.json(video);
		});
	});

	// application -------------------------------------------------------------	
	app.get('/admin', function(req, res) {
		res.sendfile('./public/admin.html');
	});
	
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});
};
